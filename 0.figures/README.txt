In this directory contains all figures cited in the paper.

Figure 2 A and B: sqrtCD4-vs-HIV-samexy-2022-10-27.png
This figure plots square root of CD4 on the x axis and log10 HIV
on the y axis. It also compares these values between the pediatric (n=21)
and adult cases (n=207).

Figure 4: Heatmap_2023-05-12.png
Human RNAseq heatmap of pediatric KS in comparison to adult KS.

pca_adult_peds_2023-05-12.png
This is a pca plot of the first four PC directions showing that the adult
samples cluster together and similarly for the pediatric samples.

volcano_prt_only_2023-05-12.png
This is a volcano plot for differential gene analysis to show which genes
express differently between the two groups: adults and pediatrics.


#' RNA seq analysis
#' output:
#'        heatmap
#'        volcano plot

#' To install from bioconductor use BiocManager::install("ComplexHeatmap")

#+ Load packages
box::use(glue[glue],
         dplyr[...],
         tidyr[pivot_longer, pivot_wider],
         matrixStats[rowSds, rowMaxs],
         stringr[str_starts],
         ComplexHeatmap[...],
         grid[gpar, unit],
         ggplot2[...],
         stringr[...],
         circlize[colorRamp2],
         DESeq2[DESeqDataSetFromMatrix, DESeq, results, lfcShrink, vst, counts,
                         estimateSizeFactors],
         SummarizedExperiment[assay],
         lubridate[today],
         sigclust[sigclust, plot],
         limma[removeBatchEffect],
         ggbreak[...],
         ./fn[...],
         here[here])



#+ Load Data
fig_dir <- here("0.figures")
ped_long <- readr::read_csv(here("2.rna_analysis/0.processed_data/ped_long.csv"), name_repair = "universal") %>%
    mutate(grp = "ped")
# cluster with good rna seq coverage
# In the reanalysis - those labled as `2` have low coverage
.ped_clust <- read.csv(here("2.rna_analysis/0.processed_data/ped_clust.csv"))
group_by(ped_long, Study_ID) |>
  summarise(total = sum(Total.gene.reads)) |>
  arrange(total) |> left_join(.ped_clust)
ped_selected <- .ped_clust |>
    filter(clust == 1)
adult_clust <- read.csv(here("2.rna_analysis/0.processed_data/adult_clust.csv"))
adult_wide <- read.csv(here("2.rna_analysis/0.processed_data/merged_95_newtrim_RPKM.csv"))
adult_wide_tc <- read.csv(here("2.rna_analysis/0.processed_data/merged_95_newtrim.csv")) %>%
    dplyr::rename(gene = "X") %>% select(!KSP005)

#+ Selecting for data pertinent to analysis

# filter for protein coding genes
protein_coding_genes <- ped_long %>% dplyr::select(Name, Biotype) %>%
    filter(Biotype == "protein_coding") %>% select(Name) %>% dplyr::rename("gene" = "Name") %>%
    unique()
ped_selected <- ped_long[ped_long$Study_ID %in% ped_selected$Study_ID, ]


#+ Reorganizing data into format for DESeq
ped_selected <- ped_selected %>% select(Name, Study_ID, RPKM, grp) %>%
    dplyr::rename("gene" = Name) %>%
    dplyr::rename("sample" = Study_ID) %>% as_tibble()

adult_long <- adult_wide %>% pivot_longer(cols = !X, names_to = "sample", values_to = "RPKM") %>%
    dplyr::rename("gene" = X) %>%
    mutate(grp = "adult")

combined_long <- rbind(ped_selected, adult_long)

#inputation of values
combined_wide <- combined_long %>% dplyr::select(-grp) %>% pivot_wider(names_from = sample, values_from = RPKM, values_fill = 0) %>%
    as.data.frame()

#Formatting for cluster analysis
mat_raw_combined <- combined_wide
rownames(mat_raw_combined) <- mat_raw_combined$gene
mat_raw_combined <- mat_raw_combined %>% dplyr::select(-gene)
mat_combined <- as.matrix(log2(mat_raw_combined + 1))

#Center by median
mat_med_combined <- t(median_center(mat_combined))

#+ Heatmap
df_cv1_combined <- data.frame(gene = rownames(mat_med_combined),
                     cv = rowSds(mat_med_combined) / rowMeans(mat_med_combined)) %>%
  arrange(-abs(cv)) %>%
  head(n = 2000L)
mat_sub_med_combined <- mat_med_combined[which(rownames(mat_med_combined) %in% df_cv1_combined$gene),]

clusters <- hc_col_cut(mat_sub_med_combined)
#removed 16 samples from first hierarchical clustering
clust_1 <- clusters$cutree %>% filter(clust == 1)

mat_sub_c1_med_combined <- mat_sub_med_combined[, colnames(mat_sub_med_combined) %in% clust_1$sample]

clusters <- hc_col_cut(mat_sub_c1_med_combined)

clust_df <- clusters$cutree %>%
  mutate(grp = case_when( str_starts(sample, "K") ~ "adult",
                          T ~ "ped"))

clust_df <- clust_df %>% filter(sample != c("KSP005","KSP044"))


sub_mat <- mat_sub_c1_med_combined[, !(colnames(mat_sub_c1_med_combined) %in% c("KSP005", "KSP044"))]

heat <- t((t(sub_mat)))
dcolor <- c("#0073C2FF", "#EFC000FF") #brewer.pal(n = 8, name = "Set1")
gcolor <- c("#DDCC77", "#888888")
myCol <- colorRampPalette(c('dodgerblue', 'black', 'yellow'))(100)
myBreaks <- seq(-1.5, 1.5, length.out = 100)

  # Create an initial data-frame of the annotation that we want to use
  # In this example, the 'ann' object turns out to be the exact same as 'metadata'
  ann <- data.frame(
      hgroup = clust_df$clust[match(colnames(sub_mat), clust_df$sample)],
      dtaset = clust_df$grp,
    stringsAsFactors = FALSE)

  # create the colour mapping
  colours <- list(
      hgroup = c('1' = dcolor[1], '2' = dcolor[2]),
      dtaset = c('ped' = gcolor[1], 'adult' = gcolor[2]))

  # now create the ComplexHeatmap annotation object
  # as most of these parameters are self-explanatory, comments will only appear where needed
  colAnn <- HeatmapAnnotation(
    df = ann,
    which = 'col', # 'col' (samples) or 'row' (gene) annotation?
    na_col = 'white', # default colour for any NA values in the annotation data-frame, 'ann'
    col = colours,
    annotation_height = 0.6,
    annotation_width = unit(1, 'cm'),
    gap = unit(1, 'mm'),
    annotation_legend_param = list(
      hgroup = list(
        nrow = 2,
        title = 'Group',
        title_position = 'topcenter',
        legend_direction = 'vertical',
        title_gp = gpar(fontsize = 12, fontface = 'bold'),
        labels_gp = gpar(fontsize = 12, fontface = 'bold')),
      dtaset = list(
        nrow = 2,
        title = 'Age',
        title_position = 'topcenter',
        legend_direction = 'vertical',
        title_gp = gpar(fontsize = 12, fontface = 'bold'),
        labels_gp = gpar(fontsize = 12, fontface = 'bold'))))

    # Performing k-means or PAM on our data can help us to identify internal ‘structure’ in the data that may relate to biologically meaningful pathways, as an example.
  pamClusters <- cluster::pam(heat, k = 4) # pre-select k = 4 centers
  pamClusters$clustering <- paste0('Cluster ', pamClusters$clustering)

  # fix order of the clusters to have 1 to 4, top to bottom
  pamClusters$clustering <- factor(pamClusters$clustering,
    levels = c('Cluster 1', 'Cluster 2', 'Cluster 3', 'Cluster 4'))

  # create the actual heatmap object
    hmap <- Heatmap(heat,

    # split the genes / rows according to the PAM clusters
      split = pamClusters$clustering,
      cluster_row_slices = FALSE,

    name = 'Gene\nRPKM\ncenter',

    col = colorRamp2(myBreaks, myCol),

    # parameters for the colour-bar that represents gradient of expression
      heatmap_legend_param = list(
        color_bar = 'continuous',
        legend_direction = 'vertical',
        legend_width = unit(8, 'cm'),
        legend_height = unit(5.0, 'cm'),
        title_position = 'topcenter',
        title_gp=gpar(fontsize = 12, fontface = 'bold'),
        labels_gp=gpar(fontsize = 12, fontface = 'bold')),

    # row (gene) parameters
      cluster_rows = TRUE,
      show_row_dend = TRUE,
      #row_title = 'Statistically significant genes',
      row_title_side = 'left',
      row_title_gp = gpar(fontsize = 12,  fontface = 'bold'),
      row_title_rot = 90,
      show_row_names = FALSE,
      row_names_gp = gpar(fontsize = 10, fontface = 'bold'),
      row_names_side = 'left',
      row_dend_width = unit(25,'mm'),

    # column (sample) parameters
      cluster_columns = TRUE,
      show_column_dend = TRUE,
      column_title = '',
      column_title_side = 'bottom',
      column_title_gp = gpar(fontsize = 12, fontface = 'bold'),
      column_title_rot = 0,
      show_column_names = FALSE,
      column_names_gp = gpar(fontsize = 10, fontface = 'bold'),
      column_names_max_height = unit(10, 'cm'),
      column_dend_height = unit(25,'mm'),

    # cluster methods for rows and columns
      clustering_distance_columns = "euclidean" ,
      clustering_method_columns = 'ward.D2',
      clustering_distance_rows = "euclidean" ,
      clustering_method_rows = 'ward.D2',

    # specify top and bottom annotations
      top_annotation = colAnn#,
     #bottom_annotation = boxplotCo
    )

    draw(hmap,# + genelabels,
    heatmap_legend_side = 'left',
    annotation_legend_side = 'right',
    row_sub_title_side = 'left')


## plotPNG(PlotObject = draw(hmap, # + genelabels,
##     heatmap_legend_side = 'left',
##     annotation_legend_side = 'right',
##     row_sub_title_side = 'left'),
##     NameOfPlot = glue("{fig_dir}/Heatmap_{today()}.png"),
##     width = 10, height = 10)


#+ Volcano plot using total.gene.counts and DESeq2
adult_long_tc <- adult_wide_tc %>%
    pivot_longer(cols = !gene, values_to = "Total.count", names_to = "Study_ID")
ped_long_tc <- ped_long[ped_long$Study_ID %in% ped_selected$sample, ] %>%
    select(Name, Study_ID, Total.gene.reads) %>%
    dplyr::rename(gene = "Name") %>%
    dplyr::rename("Total.count" = "Total.gene.reads")

#combining adults and peds for total counts (tc)
all_long_tc <- rbind(adult_long_tc, ped_long_tc)
all_wide_tc <- all_long_tc %>% pivot_wider(names_from = "Study_ID", values_from = "Total.count")

#filter for blue cluster in heatmap (cluster1
clust1_heat <- clust_df |> filter(clust == 1) |> select(sample)
all_wide_tc_1 <- all_wide_tc[,colnames(all_wide_tc) %in% clust1_heat[[1]]]

##reconfiguring df for deseq2 package
rownames_all <- all_wide_tc$gene
all_wide_tc_1 <- all_wide_tc_1 %>% as.data.frame()
rownames(all_wide_tc_1) <- rownames_all
all_wide_tc_1[is.na(all_wide_tc_1)] <- 0
all_wide_tc_1 <- all_wide_tc_1 + 1

exp_data <- data.frame("Study_ID" = colnames(all_wide_tc_1)) %>%
    dplyr::mutate(grp = case_when(str_starts(Study_ID, "P") ~ "ped",
                           str_starts(Study_ID, "K") ~ "adult",
                           TRUE ~ "ped"))


dds <- DESeqDataSetFromMatrix(as.matrix(all_wide_tc_1),
                                   colData = exp_data,
                                   design = ~ grp)


##keep <- rowSums(counts(dds)) >= 10
keep <- rowMaxs(counts(dds)) >= 10
dds <- dds[keep,]
dds <- estimateSizeFactors(dds)
mat_raw <- counts(dds, normalized=TRUE)
##mat <- log2(mat_raw + 1)
##VST normalization instead of log2
mat <- assay(vst(dds, blind=FALSE))

#select for protein coding genes as well as remove mitochondrial genes
all_wide_tc_1 <- all_wide_tc_1[which(rownames(all_wide_tc_1) %in% rownames(mat)), ]
protein_wide_tc <- all_wide_tc_1[which(rownames(all_wide_tc_1) %in% protein_coding_genes[[1]]), ]
mito_genes <- protein_coding_genes[grepl("^MT-", protein_coding_genes$gene),]
protein_wide_nomt_tc <- protein_wide_tc[which(!(rownames(protein_wide_tc) %in% mito_genes)), ]

dds <- DESeqDataSetFromMatrix(all_wide_tc_1,
                                   colData = exp_data,
                                   design = ~ grp)

dds <- DESeq(dds)
#dds$clust <- relevel(dds$clust, ref="2")
res <- results(dds)
res <- lfcShrink(dds, coef="grp_ped_vs_adult", type="apeglm")
res$abs_log2FoldChange <- abs(res$log2FoldChange)
## write.csv(res, glue("{getwd()}/2.rna_analysis/0.processed_data/adult_ped_log2_{today()}.csv"))
head(data.frame(res) %>%
     arrange(-abs(log2FoldChange)))

p_vol <- PLOTvolcano_marker(TheData = data.frame(res), marker = T,
                            lfc_cutoff_down = -8, lfc_cutoff_up = 8,
                            label_cutoff_down = -8, label_cutoff_up = 8,
                            psmall = 1e-50, xlim = c(-20, 20))

p_vol
 # plotPNG(PlotObject = p_vol + ggtitle("Pediatric vs. Adult (ref)") + theme_dirk(), NameOfPlot = glue("{fig_dir}/volcano_{today()}.png"), width = 12, height = 12)


dds <- DESeqDataSetFromMatrix(protein_wide_nomt_tc,
                                   colData = exp_data,
                                   design = ~ grp)

dds <- DESeq(dds)
#dds$clust <- relevel(dds$clust, ref="2")
res <- results(dds)
res <- lfcShrink(dds, coef="grp_ped_vs_adult", type="apeglm")
res$abs_log2FoldChange <- abs(res$log2FoldChange)
# write.csv(res, here::here("RNAseq analysis/resLFC_medgene_2000_0517_newtrim.csv"))

head(data.frame(res) %>%
  arrange(-abs(log2FoldChange)))

p_vol <- PLOTvolcano_marker(TheData = data.frame(res), marker = T,
                            lfc_cutoff_down = -8, lfc_cutoff_up = 8,
                            label_cutoff_down = -8, label_cutoff_up = 8,
                            psmall = 1e-50, xlim = c(-20, 20))

p_vol

p_vol_ann <- p_vol +
    annotate("text", x = 15, y = 28, label = "Upregulated in\nPediatrics", size = 5,
             fontface = "bold") +
    annotate("text", x = -15, y = 28, label = "Downregulated\nin Pediatrics", size = 5,
             fontface = "bold")
p_vol_ann

 # plotPNG(PlotObject = p_vol_ann + ggtitle("Pediatric vs. Adult (ref)") + theme_dirk(), NameOfPlot = glue("{fig_dir}/volcano_prt_only_{today()}.png"), width = 12, height = 12)


 # write.csv((p_vol$data |> filter(log2FoldChange >=8 & padj <= 0.05)), here("2.rna_analysis/0.processed_data/vol_genes_forIPA.csv"))


##test for same genes exist in adult and peds
Res <- as.data.frame(data.frame(res))

lfc_cutoff_down = -8
lfc_cutoff_up = 8
label_cutoff_down = -8
label_cutoff_up = 8
psmall = 1e-50

# up <- subset(Res, padj < psmall & log2FoldChange > min(abs(label_cutoff_down), label_cutoff_up))
# down <- subset(Res, padj < psmall & log2FoldChange < -min(abs(label_cutoff_down), label_cutoff_up))
clust_df <- clust_df |> dplyr::rename("names" = "sample")

grp_numeric <- clust_df %>% select(names, grp) %>% dplyr::rename("clust" = "grp")
grp_numeric$clust <- as.factor(grp_numeric$clust)
grp_numeric <- grp_numeric %>% mutate(numeric = case_when(clust == "adult" ~ 2,
                                          clust == "ped" ~ 1))


#+ pca plot

## png(glue("{fig_dir}/pca_adult_peds_{today()}.png"), width = 600, height = 600)
pca_figure_clust(t(sub_mat), grp_numeric, label_limit = list(PC1 = c(-Inf, Inf), PC2 = c(-Inf, 20),
                                                        PC3 = c(-Inf, 20), PC4 = c(-Inf, Inf)))
##  dev.off()
## plotPNG(PlotObject = pca, NameOfPlot = glue("{fig_dir}/pca_adult_peds_{today()}.png"), width = 12, height = 12)

pca <- prcomp(t(sub_mat))
summary(pca)

all(clust_df |> select(names) == colnames(sub_mat))


clust1_sub_mat <- sub_mat[,colnames(sub_mat) %in% (clust_df |> select(names, clust) |> filter(clust == 1) |> select(names) |> as.vector())[[1]]]

sig_clust <- sigclust(t(clust1_sub_mat), nsim = 1000, nrep = 1, labflag = 1,
                      label = grp_numeric[rownames(grp_numeric) %in%
                                          (clust_df |> select(names, clust) |>
                                           filter(clust == 1) |> rownames()), ]$numeric)


# png(glue("{fig_dir}/sig_clust_{today()}.png"), width = 600, height = 600)
plot_sig_clus <- plot(sig_clust)
# dev.off()
## plotPNG(PlotObject = plot_sig_clus, NameOfPlot = glue("{fig_dir}/sig_clust_{today()}.png"), width = 12, height = 12)

batch <- grp_numeric[rownames(grp_numeric) %in% (clust_df |> select(names, clust) |>
                                                 filter(clust == 1) |> rownames()), ]$numeric

batch_corrected_clust1_sub_mat <- removeBatchEffect(clust1_sub_mat, batch)

batch_corrected_sig_clust <- sigclust(t(batch_corrected_clust1_sub_mat), nsim = 1000,
                                     nrep = 1, labflag = 1,
                                     label = batch)
plot(batch_corrected_sig_clust)


#+ Revisions
all_wide_tc_92 <- all_wide_tc[, colnames(sub_mat)] |> as.data.frame()
rownames(all_wide_tc_92) <- all_wide_tc$gene
all_wide_tc_92 <- all_wide_tc_92 + 1

all_wide_tc_92 <- all_wide_tc_92[which(rownames(all_wide_tc_92) %in% rownames(mat)), ]
protein_wide_tc <- all_wide_tc_92[which(rownames(all_wide_tc_92) %in% protein_coding_genes[[1]]), ]
mito_genes <- protein_coding_genes[grepl("^MT-", protein_coding_genes$gene),]
protein_wide_nomt_tc <- protein_wide_tc[which(!(rownames(protein_wide_tc) %in% mito_genes)), ]


#+ Check for negative binomial distribution
protein_wide_nomt_tc$genes <- rownames(protein_wide_nomt_tc)

protein_wide_nomt_tc |> as.data.frame() |> pivot_longer(cols = !genes) |>
    filter(!str_detect(name, "KSP")) |>
    ggplot(aes(x = value)) +
    ## scale_y_continuous(break = seq())
    xlim(c(0,300)) + 
    geom_histogram(bins = 300) +
    facet_wrap(vars(name), nrow = 3, scales = "free")
## ggsave(glue("{fig_dir}/negative_binomial_hist_{today()}.png"), device = "png", units = "in", width = 12, height = 5)


#+ VST heatmap
dds <- DESeqDataSetFromMatrix(as.matrix(na.omit(protein_wide_nomt_tc)),
                                   colData = clust_df,
                                   design = ~ 1)


##keep <- rowSums(counts(dds)) >= 10
keep <- rowMaxs(counts(dds)) >= 10
dds <- dds[keep,]
dds <- estimateSizeFactors(dds)
mat_raw <- counts(dds, normalized=TRUE)
##mat <- log2(mat_raw + 1)
##VST normalization instead of log2
mat <- assay(vst(dds, blind=FALSE))

#Center by median
mat_med_combined <- t(median_center(mat))

df_cv1_combined <- data.frame(gene = rownames(mat_med_combined),
                     cv = rowSds(mat_med_combined) / rowMeans(mat_med_combined)) %>%
  arrange(-abs(cv)) %>%
  head(n = 2000L)
mat_sub_med_combined <- mat_med_combined[which(rownames(mat_med_combined) %in% df_cv1_combined$gene),]

clusters_vst <- hc_col_cut(mat_sub_med_combined)

clust_df_vst <- clusters_vst$cutree %>%
  mutate(grp = case_when( str_starts(sample, "K") ~ "adult",
                          T ~ "ped"))

clust_df_vst <- clust_df_vst %>% filter(sample != c("KSP005","KSP044"))

heat <- t((t(mat_sub_med_combined)))
dcolor <- c("#0073C2FF", "#EFC000FF") #brewer.pal(n = 8, name = "Set1")
gcolor <- c("#DDCC77", "#888888")
myCol <- colorRampPalette(c('dodgerblue', 'black', 'yellow'))(100)
myBreaks <- seq(-1.5, 1.5, length.out = 100)

  # Create an initial data-frame of the annotation that we want to use
  # In this example, the 'ann' object turns out to be the exact same as 'metadata'
  ann <- data.frame(
      hgroup = clust_df_vst$clust[match(colnames(mat_sub_med_combined), clust_df_vst$sample)],
      dtaset = clust_df_vst$grp,
    stringsAsFactors = FALSE)

  # create the colour mapping
  colours <- list(
      hgroup = c('1' = dcolor[1], '2' = dcolor[2]),
      dtaset = c('ped' = gcolor[1], 'adult' = gcolor[2]))

  # now create the ComplexHeatmap annotation object
  # as most of these parameters are self-explanatory, comments will only appear where needed
  colAnn <- HeatmapAnnotation(
    df = ann,
    which = 'col', # 'col' (samples) or 'row' (gene) annotation?
    na_col = 'white', # default colour for any NA values in the annotation data-frame, 'ann'
    col = colours,
    annotation_height = 0.6,
    annotation_width = unit(1, 'cm'),
    gap = unit(1, 'mm'),
    annotation_legend_param = list(
      hgroup = list(
        nrow = 2,
        title = 'Group',
        title_position = 'topcenter',
        legend_direction = 'vertical',
        title_gp = gpar(fontsize = 12, fontface = 'bold'),
        labels_gp = gpar(fontsize = 12, fontface = 'bold')),
      dtaset = list(
        nrow = 2,
        title = 'Age',
        title_position = 'topcenter',
        legend_direction = 'vertical',
        title_gp = gpar(fontsize = 12, fontface = 'bold'),
        labels_gp = gpar(fontsize = 12, fontface = 'bold'))))

    # Performing k-means or PAM on our data can help us to identify internal ‘structure’ in the data that may relate to biologically meaningful pathways, as an example.
  pamClusters <- cluster::pam(heat, k = 4) # pre-select k = 4 centers
  pamClusters$clustering <- paste0('Cluster ', pamClusters$clustering)

  # fix order of the clusters to have 1 to 4, top to bottom
  pamClusters$clustering <- factor(pamClusters$clustering,
    levels = c('Cluster 1', 'Cluster 2', 'Cluster 3', 'Cluster 4'))

  # create the actual heatmap object
    hmap <- Heatmap(heat,

    # split the genes / rows according to the PAM clusters
      split = pamClusters$clustering,
      cluster_row_slices = FALSE,

    name = 'Gene\nVST\ncenter',

    col = colorRamp2(myBreaks, myCol),

    # parameters for the colour-bar that represents gradient of expression
      heatmap_legend_param = list(
        color_bar = 'continuous',
        legend_direction = 'vertical',
        legend_width = unit(8, 'cm'),
        legend_height = unit(5.0, 'cm'),
        title_position = 'topcenter',
        title_gp=gpar(fontsize = 12, fontface = 'bold'),
        labels_gp=gpar(fontsize = 12, fontface = 'bold')),

    # row (gene) parameters
      cluster_rows = TRUE,
      show_row_dend = TRUE,
      #row_title = 'Statistically significant genes',
      row_title_side = 'left',
      row_title_gp = gpar(fontsize = 12,  fontface = 'bold'),
      row_title_rot = 90,
      show_row_names = FALSE,
      row_names_gp = gpar(fontsize = 10, fontface = 'bold'),
      row_names_side = 'left',
      row_dend_width = unit(25,'mm'),

    # column (sample) parameters
      cluster_columns = TRUE,
      show_column_dend = TRUE,
      column_title = '',
      column_title_side = 'bottom',
      column_title_gp = gpar(fontsize = 12, fontface = 'bold'),
      column_title_rot = 0,
      show_column_names = FALSE,
      column_names_gp = gpar(fontsize = 10, fontface = 'bold'),
      column_names_max_height = unit(10, 'cm'),
      column_dend_height = unit(25,'mm'),

    # cluster methods for rows and columns
      clustering_distance_columns = "euclidean" ,
      clustering_method_columns = 'ward.D2',
      clustering_distance_rows = "euclidean" ,
      clustering_method_rows = 'ward.D2',

    # specify top and bottom annotations
      top_annotation = colAnn#,
     #bottom_annotation = boxplotCo
    )

    draw(hmap,# + genelabels,
    heatmap_legend_side = 'left',
    annotation_legend_side = 'right',
    row_sub_title_side = 'left')


## plotPNG(PlotObject = draw(hmap, # + genelabels,
##     heatmap_legend_side = 'left',
##     annotation_legend_side = 'right',
##     row_sub_title_side = 'left'),
##     NameOfPlot = glue("{fig_dir}/Heatmap_vst_{today()}.png"),
##     width = 10, height = 10)



